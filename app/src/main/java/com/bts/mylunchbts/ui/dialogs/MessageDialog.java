package com.bts.mylunchbts.ui.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bts.mylunchbts.R;
import com.bts.mylunchbts.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MessageDialog extends AppCompatDialogFragment {

    @BindView(R.id.title)
    TextView edtTitle;
    @BindView(R.id.message)
    TextView edtMessage;

    public static MessageDialog newInstance(@NonNull String title, @NonNull String message) {

        Bundle args = new Bundle();
        args.putString(Constants.KEY_TITLE, title);
        args.putString(Constants.KEY_MESSAGE, message);

        MessageDialog fragment = new MessageDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setStyle(DialogFragment.STYLE_NO_TITLE, R.style.AppTheme_Dialog);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_message, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Dialog dialog = getDialog();
        if (dialog != null && dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        ButterKnife.bind(this, view);

        String title = getArguments().getString(Constants.KEY_TITLE);
        String message = getArguments().getString(Constants.KEY_MESSAGE);

        edtTitle.setText(title);
        edtMessage.setText(message);

    }

    @OnClick(R.id.btnOK)
    public void onOkClicked(){
        dismiss();
    }

}

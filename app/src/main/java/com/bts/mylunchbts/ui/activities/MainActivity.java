package com.bts.mylunchbts.ui.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bts.mylunchbts.R;
import com.bts.mylunchbts.mvp.presenters.LoginPresenter;
import com.bts.mylunchbts.mvp.views.LoginView;
import com.bts.mylunchbts.ui.dialogs.MessageDialog;
import com.bts.mylunchbts.ui.dialogs.ProgressDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements LoginView {


    @BindView(R.id.edit_name)
    EditText edtName;
    @BindView(R.id.edit_email)
    EditText edtEmail;
    @BindView(R.id.edit_password)
    EditText edtPassword;
    @BindView(R.id.edit_confirm_password)
    EditText edtConfirmPassword;

    private LoginPresenter presenter;
    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            FloatingActionButton fab = findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });

            ButterKnife.bind(this);

            presenter = new LoginPresenter();
            presenter.attachMvpView(this);
            firebaseAuth = FirebaseAuth.getInstance();
            progressDialog = ProgressDialog.newInstance();

        }


    @Override
    public void onStart() {
        super.onStart();
    }


        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_main, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                return true;
            }

            return super.onOptionsItemSelected(item);
        }

    @OnClick(R.id.btn_login)
    public void onLoginClickListener() {

        presenter.validateFields(edtEmail.getText().toString().trim(), edtPassword.getText().toString().trim());

        }


    @Override
    public void onEmptyEmail() {
     edtEmail.setError(getString(R.string.login_error_empty_email));
    }

    @Override
    public void onEmptyPassword() {
        edtPassword.setError(getString(R.string.login_error_empty_password));
    }

    @Override
    public void onWrongPassword() {
        edtPassword.setError(getString(R.string.login_error_wrong_password));
    }

    @Override
    public void onFormValidated() {

        progressDialog.show(getSupportFragmentManager(), progressDialog.getClass().getName());

            firebaseAuth.createUserWithEmailAndPassword(edtEmail.getText().toString().trim(),edtPassword.getText().toString().trim() )
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //checking if success
                        if(task.isSuccessful()){
                            onUserCreatedSuccesfull();

                        }else{
                            onUserNotCreated();
                        }
                        progressDialog.dismiss();
                    }
                });

        }

    @Override
    public void onUserCreatedSuccesfull() {
        progressDialog.dismiss();
        MessageDialog.newInstance(getString(R.string.login_register_title), getString(R.string.login_register_message_ok)).show(this.getSupportFragmentManager(),MessageDialog.class.getName());

    }

    @Override
    public void onUserNotCreated() {
        progressDialog.dismiss();
        MessageDialog.newInstance(getString(R.string.login_register_title), getString(R.string.login_register_message_error)).show(this.getSupportFragmentManager(),MessageDialog.class.getName());

    }

    @Override
    public Context getMvpContext() {
        return this;
    }

    @Override
    public void onNoInternetConnection() {
        Snackbar snackbar = Snackbar.make(getCurrentFocus(), R.string.server_no_internet, Snackbar.LENGTH_INDEFINITE); //check View!

        View snackbarView = snackbar.getView();
        TextView textView = snackbarView.findViewById(R.id.snackbar_text);
        textView.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        snackbar.setAction(R.string.retry, new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        snackbar.show();
    }

    @Override
    public void onErrorCode(String message) {

    }
}




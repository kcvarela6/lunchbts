package com.bluetrailsoft.androidscaffolding.listeners;

import android.view.View;

public interface OnItemClickListener {
    void onItemClickListener(View view);
}

package com.bts.mylunchbts.utils;

public class Constants {

    public static final String KEY_TITLE = "title";
    public static final String KEY_MESSAGE = "message";
    public static final int PASSWORD_LENGTH = 6;
}

package com.bts.mylunchbts.mvp.presenters;

import com.bts.mylunchbts.mvp.views.LoginView;
import com.bts.mylunchbts.utils.Constants;
import com.google.firebase.auth.FirebaseAuth;


public class LoginPresenter extends BasePresenter<LoginView> {

        private FirebaseAuth mAuth;


public void validateFields(String username, String password) {

        boolean validated = true;

        if (username == null || username.isEmpty()) {
        getMvpView().onEmptyEmail();
        validated = false;
        }

        if (password == null || password.isEmpty()) {
        getMvpView().onEmptyPassword();
        validated = false;
        }


        if (password.length() < Constants.PASSWORD_LENGTH) {
                getMvpView().onWrongPassword();
                validated = false;
        }

        if (validated) {
        getMvpView().onFormValidated();
        }
        }




}

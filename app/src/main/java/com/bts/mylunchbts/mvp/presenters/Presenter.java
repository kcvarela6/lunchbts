package com.bts.mylunchbts.mvp.presenters;



public interface Presenter<MvpView> {

    void attachMvpView(MvpView var1);

    void dettachMvpView();
}

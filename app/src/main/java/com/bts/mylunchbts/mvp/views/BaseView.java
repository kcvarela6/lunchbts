package com.bts.mylunchbts.mvp.views;

import android.content.Context;

/**
 * Created by disis on 15/01/18.
 */

public interface BaseView {

    Context getMvpContext();

    void onNoInternetConnection();

    void onErrorCode(String message);
}

package com.bts.mylunchbts.mvp.presenters;

import com.bts.mylunchbts.mvp.views.BaseView;
import io.reactivex.disposables.CompositeDisposable;


public class BasePresenter<T extends BaseView> implements Presenter <T>{

    protected T mvpView;
    protected CompositeDisposable compositeSubscription;

    public BasePresenter() {
    }

    public void attachMvpView(T t) {
        this.mvpView = t;
        this.compositeSubscription = new CompositeDisposable();
    }

    public void dettachMvpView() {
        this.mvpView = null;
        if (this.compositeSubscription != null) {
            this.compositeSubscription.clear();
        }

    }

    public T getMvpView() {
        return this.mvpView;
    }
}

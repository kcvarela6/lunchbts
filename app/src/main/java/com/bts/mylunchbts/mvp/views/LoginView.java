package com.bts.mylunchbts.mvp.views;



public interface LoginView extends BaseView {

    void onEmptyEmail();

    void onEmptyPassword();

    void onWrongPassword();

    void onFormValidated();

    void onUserCreatedSuccesfull();

    void  onUserNotCreated();

}
